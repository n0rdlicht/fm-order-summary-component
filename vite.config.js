import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
      /**
       * Storybook (specifically the interactions addon) requires that we use their
       *   instrumented version of jest-expect. So our storybook does so. To make
       *   these interactions still work in vitest we have @storybook/jest aliased
       *   to resolve to vitest which, critically, exports { expect } as well.
       * See https://github.com/storybookjs/storybook/issues/17326
       */
    },
  },
  build: {
    outDir: "public",
  },
  test: {
    environment: "jsdom",
    reporters: "json",
    outputFile: "./tests/output.json",
    coverage: {
      reporter: ["text", "cobertura"],
    },
  },
});
