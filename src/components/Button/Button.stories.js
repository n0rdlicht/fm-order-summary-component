import Button from "./Button.vue";
import vitestResults from "../../../tests/output.json";

// using jest as long as https://github.com/storybookjs/storybook/issues/17326 is not resolved
// import { expect } from "@storybook/jest";
// import { userEvent, waitFor, within } from "@storybook/testing-library";

export default {
  title: "Atoms/Button",
  component: Button,
  argTypes: {
    kind: { control: "inline-radio", options: ["submit", "cancel", "change"] },
    onClick: { action: true },
    onMouseover: { action: true },
  },
  parameters: {
    vitest: {
      testFile: "Button.spec.js",
      testResults: vitestResults,
    },
    actions: {
      handles: ["click button"],
    },
  },
};

const Template = (args) => ({
  components: { Button },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<Button v-bind="args" />',
});

export const Submit = Template.bind({});
Submit.args = {
  kind: "submit",
  label: "Submit",
};

export const SubmitHover = Template.bind({});
SubmitHover.parameters = {
  pseudo: { hover: true },
};
SubmitHover.args = {
  kind: "submit",
  label: "Submit",
};

// export const Submitted = Template.bind({});
// Submitted.args = {
//   kind: "submit",
//   label: "Submit",
// };
// Submitted.play = async ({ args, canvasElement }) => {
//   const canvas = within(canvasElement);

//   await userEvent.click(canvas.getByRole("button"));

//   await waitFor(() => expect(args.onClick).toHaveBeenCalled());
// };

export const Cancel = Template.bind({});
Cancel.args = {
  kind: "cancel",
  label: "Cancel",
};

export const Change = Template.bind({});
Change.args = {
  kind: "change",
  label: "Change",
};
