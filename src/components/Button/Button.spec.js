import { describe, it, expect } from "vitest";
import { render } from "@testing-library/vue";
import userEvent from "@testing-library/user-event";

import Button from "./Button.vue";

describe("button", () => {
  it("renders submit", async () => {
    const user = userEvent.setup();
    const wrapper = render(Button, {
      props: {
        kind: "submit",
        label: "Submit",
      },
    });
    expect(wrapper.getByRole("button", { name: "submit" })).toBeDefined();

    await user.click(wrapper.getByRole("button", { name: "submit" }));
  });

  it("renders cancel", async () => {
    const user = userEvent.setup();
    const wrapper = render(Button, {
      props: {
        kind: "cancel",
        label: "Cancel",
      },
    });
    expect(wrapper.getByRole("button", { name: "cancel" })).toBeDefined();

    await user.click(wrapper.getByRole("button", { name: "cancel" }));
  });

  it("renders change", async () => {
    const user = userEvent.setup();
    const wrapper = render(Button, {
      props: {
        kind: "change",
        label: "Change",
      },
    });
    expect(wrapper.getByRole("button", { name: "change" })).toBeDefined();

    await user.click(wrapper.getByRole("button", { name: "change" }));
  });
});
