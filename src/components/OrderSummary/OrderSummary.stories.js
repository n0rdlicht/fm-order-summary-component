import OrderSummary from "./OrderSummary.vue";
import vitestResults from "../../../tests/output.json";

export default {
  title: "Modals/OrderSummary",
  component: OrderSummary,
  parameters: {
    vitest: {
      testFile: "OrderSummary.spec.js",
      testResults: vitestResults,
    },
    actions: {
      handles: ["click button"],
    },
    chromatic: { viewports: [320, 1200] },
  },
};

const Template = (args, { argTypes }) => ({
  components: { OrderSummary },
  setup() {
    //👇 The args will now be passed down to the template
    return { args };
  },
  template: '<OrderSummary v-bind="args" />',
});

export const Default = Template.bind({});
Default.args = {
  orderDescription:
    "You can now listen to millions of songs, audiobooks, and podcasts on any device anywhere you like!",
  order: {
    kind: "music",
    type: "annual",
    price: 5999,
    currency: "$",
  },
};

export const HoverStates = Template.bind({});
HoverStates.args = {
  orderDescription:
    "You can now listen to millions of songs, audiobooks, and podcasts on any device anywhere you like!",
  order: {
    kind: "music",
    type: "annual",
    price: 5999,
    currency: "$",
  },
};
HoverStates.parameters = {
  pseudo: { hover: true },
};
