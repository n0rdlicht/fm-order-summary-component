import { describe, it, expect, beforeEach, afterEach } from "vitest";
import { render, fireEvent } from "@testing-library/vue";
// import { mount } from "@vue/test-utils";

import OrderSummary from "./OrderSummary.vue";

let wrapper;

beforeEach(() => {
  wrapper = render(OrderSummary, {});
});

afterEach(() => {});

describe("order summary", () => {
  it("shows card", () => {
    expect(
      wrapper.getByRole("img", { name: "checkout graphic" })
    ).toBeDefined();
    expect(wrapper.getByText("Order Summary")).toBeDefined();
    expect(wrapper.getByRole("button", { name: "submit" })).toBeDefined();
    expect(wrapper.getByRole("button", { name: "cancel" })).toBeDefined();
  });
});
