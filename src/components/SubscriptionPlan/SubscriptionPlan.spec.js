import { describe, it, expect, beforeEach, afterEach } from "vitest";
import { render, fireEvent } from "@testing-library/vue";

import SubscriptionPlan from "./SubscriptionPlan.vue";

describe("subscription plan", () => {
  it("shows chosen plan", () => {
    const wrapper = render(SubscriptionPlan, {
      props: {
        kind: "music",
        type: "annual",
        price: 59.99,
        currency: "$",
      },
    });
    expect(wrapper.getByRole("button", { name: "change" })).toBeDefined();
    expect(wrapper.getByText("Annual Plan")).toBeDefined();
  });
  it("renders price correctly", () => {
    const wrapper = render(SubscriptionPlan, {
      props: {
        kind: "music",
        type: "annual",
        price: 59.99,
        currency: "$",
      },
    });
    expect(wrapper.findByText("$59.99/year")).toBeDefined();
  });
});
