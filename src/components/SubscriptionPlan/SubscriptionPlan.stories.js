import SubscriptionPlan from "./SubscriptionPlan.vue";
import vitestResults from "../../../tests/output.json";

export default {
  title: "Molecules/SubscriptionPlan",
  component: SubscriptionPlan,
  argTypes: {
    kind: { control: "select", options: ["music"] },
    type: { control: "inline-radio", options: ["annual", "monthly"] },
    currency: { control: "select", options: ["$"] },
  },
  parameters: {
    vitest: {
      testFile: "SubscriptionPlan.spec.js",
      testResults: vitestResults,
    },
    actions: {
      handles: ["click button"],
    },
    chromatic: { viewports: [320, 1200] },
  },
};

export const Annual = {
  args: {
    kind: "music",
    type: "annual",
    price: 5999,
    currency: "$",
  },
};

export const Monthly = {
  args: {
    kind: "music",
    type: "monthly",
    price: 5999,
    currency: "$",
  },
};
